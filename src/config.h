#pragma once
#ifdef DEVICE_NATIVE
#define USE_MAIN
#endif

#ifdef DEVICE_VALVE
#endif

#ifdef ESP8266
#define USE_EEPROM_CONFIGADAPTOR
#endif

#ifdef STM32WL55JC1
#define USE_STM32_CONFIGADAPTOR
#endif
