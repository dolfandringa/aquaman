#include "config.h"

#ifdef USE_MAIN
void setup();
void loop();
#endif


#ifdef DEVICE_VALVE
#include <ValveController.h>
#endif
#ifdef DEVICE_WEATHERSTATION
#include <WeatherStation.h>
#endif
#ifdef DEVICE_PRESSURESENSOR
#include <PressureSensor.h>
#endif

