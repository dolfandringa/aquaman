#pragma once
#include <ConfigManager.h>
#include <unity.h>

namespace test_configmanager {
    void test_initialized_new_address(void) {
        ConfigManager config;
        TEST_ASSERT_EQUAL(254, config.getOwnAddress());
    }

    void test_setOwnAddress(void) {
        ConfigManager config;
        TEST_ASSERT_EQUAL(254, config.getOwnAddress());
        config.setOwnAddress(10);
        TEST_ASSERT_EQUAL(10, config.getOwnAddress());
    }

    void test_getFirstFreeAddress(void) {
        ConfigManager config;
        TEST_ASSERT_EQUAL(1, config.getFirstFreeAddress());
        for(int i=1;i<253;i++) {
            config.setAddressInUse(i);
        }
        TEST_ASSERT_EQUAL(253, config.getFirstFreeAddress());
        config.setAddressInUse(253);
        TEST_ASSERT_EQUAL(-1, config.getFirstFreeAddress());

    }

    void test_setAddressInUse(void) {
        ConfigManager config;
        TEST_ASSERT_EQUAL(1, config.getFirstFreeAddress());
        config.setAddressInUse(1);
        TEST_ASSERT_EQUAL(2, config.getFirstFreeAddress());
        config.setAddressInUse(2);
        TEST_ASSERT_EQUAL(3, config.getFirstFreeAddress());
    }

    void test_freeAddress(void) {
        ConfigManager config;
        TEST_ASSERT_EQUAL(1, config.getFirstFreeAddress());
        config.setAddressInUse(1);
        TEST_ASSERT_EQUAL(2, config.getFirstFreeAddress());
        config.freeAddress(1);
        TEST_ASSERT_EQUAL(1, config.getFirstFreeAddress());
    }

    int main(int argc, char **argv) {
        UNITY_BEGIN();
        RUN_TEST(test_initialized_new_address);
        RUN_TEST(test_getFirstFreeAddress);
        RUN_TEST(test_setAddressInUse);
        RUN_TEST(test_freeAddress);
        RUN_TEST(test_setOwnAddress);
        UNITY_END();

        return 0;
    }
}
