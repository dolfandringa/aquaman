#include <ArduinoFake.h>
#include <unity.h>
#include <iostream>

// Horribly ugly and gruesome trick to make the private attributes testable.
// Applies to all classes/header files included below it.
#define private public
#include <MeshGateway.h>



using namespace fakeit;

namespace test_meshgateway {
    
    void test_begin(void) {
        MeshGateway gw;
        Mock<RHMesh> spy(*gw._manager);
        Spy(Method(spy, init));
        gw.begin();
        Verify(Method(spy, init));
    }

    void test_loop(void) {
        MeshGateway gw;
        Mock<RHMesh> mock(*gw._manager);
        Spy(Method(mock, recvfromAck));
        When(Method(mock, recvfromAck)).Do([](
            uint8_t* buff, uint8_t* len, uint8_t* source, uint8_t* dest, uint8_t* id, uint8_t* flags
        )->bool{
                strcpy((char *)buff, "hello");
                return true;
        });
        gw.loop();
        TEST_ASSERT_EQUAL_STRING("hello", gw._buf);
    }

    int main(int argc, char **argv) {
        When(Method(ArduinoFake(Serial), available)).Return(1);
        When(OverloadedMethod(ArduinoFake(Serial), print, size_t(const char*))).Return(1);
        When(OverloadedMethod(ArduinoFake(Serial), print, size_t(uint8_t, int))).Return(1);
        When(OverloadedMethod(ArduinoFake(Serial), println, size_t(const char*))).Return(1);
        UNITY_BEGIN();
        RUN_TEST(test_begin);
        RUN_TEST(test_loop);
        UNITY_END();
        return 0;    
    }
}
