#include <unity.h>
#include "test_configmanager.hpp"
#include "test_meshgateway.hpp"

int main(int argc, char **argv) {
    test_configmanager::main(argc, argv);
    test_meshgateway::main(argc, argv);
    return 0;
}
