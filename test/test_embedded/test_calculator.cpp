#include <Arduino.h> 
#include "calculator.h"
#include "unity.h"

#ifdef UNIT_TEST

Calculator calc;

void test_calculator_add(void) {
    TEST_ASSERT_EQUAL(10, calc.add(5, 5));
}

void setup() {
    UNITY_BEGIN();
    RUN_TEST(test_calculator_add);
    UNITY_END();
 }

void loop() {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(100);
    digitalWrite(LED_BUILTIN, LOW);
    delay(100);
}

#endif
