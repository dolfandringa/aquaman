# Aquaman

Aquaman is a water management system that measures and controls water tank levels by automatically opening/closing gate valves based on the measurement. I can send out alerts to a mobile app when water levels get dangerously low.