#pragma once
// RHMesh.h
//
// Author: Mike McCauley (mikem@airspayce.com)
// Copyright (C) 2011 Mike McCauley
// $Id: RHMesh.h,v 1.16 2018/09/23 23:54:01 mikem Exp $

#include <Arduino.h>
#define D0 0
#define D1 1
#define D2 2
#define D3 3
#define D4 4
#define D5 5
#define D6 6
#define D7 7
#define D8 8


#define RH_MESH_MAX_MESSAGE_LEN 254


// Timeout for address resolution in milliecs
#define RH_MESH_ARP_TIMEOUT 4000

class RHGenericDriver {
    public:
        RHGenericDriver();
        virtual ~RHGenericDriver();
};

class RHMesh {
    public:
        RHMesh(RHGenericDriver&, uint8_t);
        virtual ~RHMesh();
        virtual bool init();
        virtual uint8_t sendtoWait(uint8_t* buf, uint8_t len, uint8_t dest, uint8_t flags = 0);
        virtual bool recvfromAck(uint8_t* buf, uint8_t* len, uint8_t* source = NULL, uint8_t* dest = NULL, uint8_t* id = NULL, uint8_t* flags = NULL);
        virtual bool recvfromAckTimeout(uint8_t* buf, uint8_t* len,  uint16_t timeout, uint8_t* source = NULL, uint8_t* dest = NULL, uint8_t* id = NULL, uint8_t* flags = NULL);
};
