// RH_NRF24.h
// Author: Mike McCauley
// Copyright (C) 2012 Mike McCauley
// $Id: RH_NRF24.h,v 1.20 2017/07/25 05:26:50 mikem Exp $
//

#ifndef RH_NRF24_h
#define RH_NRF24_h
#include <Arduino.h>
#include "RHMesh.h"

// This is the maximum number of bytes that can be carried by the nRF24.
// We use some for headers, keeping fewer for RadioHead messages
#define RH_NRF24_MAX_PAYLOAD_LEN 32

// The length of the headers we add.
// The headers are inside the nRF24 payload
#define RH_NRF24_HEADER_LEN 4

// This is the maximum RadioHead user message length that can be supported by this library. Limited by
// the supported message lengths in the nRF24
#define RH_NRF24_MAX_MESSAGE_LEN (RH_NRF24_MAX_PAYLOAD_LEN-RH_NRF24_HEADER_LEN)

class RH_NRF24: public RHGenericDriver {
    public:
        RH_NRF24(uint8_t, uint8_t);
        RH_NRF24(int, int);
        RH_NRF24(char, char);
        virtual ~RH_NRF24();

};
#endif 
