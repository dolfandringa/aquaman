#include "PressureSensor.h"


MeshClient mesh_client;

void setup()
{
  Serial.begin(115200);
  Serial.println("Beginning mesh client.");
  mesh_client.begin();

}

void loop()
{
    mesh_client.loop();
    mesh_client.send("Test", 1);
}

