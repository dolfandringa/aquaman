#pragma once
#include <Arduino.h>
#include "ConfigData.h"
class BaseConfigAdaptor {
    public:
        BaseConfigAdaptor(){};
        virtual void sync(ConfigData &data) = 0;
        virtual void begin(ConfigData *data) = 0;
};
