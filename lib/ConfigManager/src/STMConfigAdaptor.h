#pragma once
#ifdef USE_STM_CONFIGADAPTOR
#include "BaseConfigAdaptor.h"
#include <EEPROM.h>

class STMConfigAdaptor: public BaseConfigAdaptor {
    public:
        STMConfigAdaptor();
        void begin(ConfigData *data);
        void sync(ConfigData &data);
};
#endif
