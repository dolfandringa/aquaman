#pragma once
#include <Arduino.h>
#define INITIALIZED_FLAG 18723
#define MAX_ADDRESS 254

typedef struct {
    unsigned int initialized;
    uint8_t address;
    bool addressesInUse[MAX_ADDRESS+1];
} ConfigData;
