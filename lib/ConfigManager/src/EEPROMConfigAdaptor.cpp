#ifdef USE_EEPROM_CONFIGADAPTOR
#include "EEPROMConfigAdaptor.h"
#include "BaseConfigAdaptor.h"

EEPROMConfigAdaptor::EEPROMConfigAdaptor() {
    EEPROM.begin(512);
}
void EEPROMConfigAdaptor::begin(ConfigData *data) {
    EEPROM.get(0, data);
}

void EEPROMConfigAdaptor::sync(ConfigData &data) {
    EEPROM.put(0, data);
}
#endif
