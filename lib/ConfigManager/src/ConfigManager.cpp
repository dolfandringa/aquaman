#include "ConfigManager.h"
#ifdef USE_EEPROM_CONFIGADAPTOR
EEPROMConfigAdaptor adaptor;
#endif
#ifdef USE_STM_CONFIGADAPTOR
STMConfigAdaptor adaptor;
#endif

ConfigManager::ConfigManager() {
    adaptor.begin(&data);
    if(data.initialized != INITIALIZED_FLAG)
        ConfigManager::initializeData();
}

void ConfigManager::initializeData() {
    this->data.address = MAX_ADDRESS;
    this->data.addressesInUse[0] = true;
    this->data.addressesInUse[MAX_ADDRESS] = true;
    this->data.initialized = INITIALIZED_FLAG; // Random int. Just a flag that it has been initialized.
    adaptor.sync(this->data);
}


uint8_t ConfigManager::getOwnAddress() {
    return this->data.address;
}


void ConfigManager::setOwnAddress(uint8_t address) {
    this->data.address = address;
    adaptor.sync(this->data);
}

void ConfigManager::setAddressInUse(uint8_t address) {
    if(address > MAX_ADDRESS - 1)
        return;
    
    this->data.addressesInUse[address] = true;
}

void ConfigManager::freeAddress(uint8_t address) {
    if(address > MAX_ADDRESS - 1)
        return;
    this->data.addressesInUse[address] = false;
}

int16_t ConfigManager::getFirstFreeAddress() {
    for(uint8_t i=0;i<=MAX_ADDRESS;i++){
        if(!this->data.addressesInUse[i]) 
            return i;
    }

    return -1;
        
}

ConfigManager config;
