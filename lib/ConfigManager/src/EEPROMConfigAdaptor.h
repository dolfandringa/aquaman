#pragma once
#ifdef USE_EEPROM_CONFIGADAPTOR
#include "BaseConfigAdaptor.h"
#include <EEPROM.h>


class EEPROMConfigAdaptor: public BaseConfigAdaptor {
    public:
        EEPROMConfigAdaptor();
        void begin(ConfigData *data);
        void sync(ConfigData &data);
};
#endif
