#ifdef USE_STM_CONFIGADAPTOR
#include "STMConfigAdaptor.h"

STMConfigAdaptor::STMConfigAdaptor() {
    EEPROM.begin();
}

void STMConfigAdaptor::begin(ConfigData *data) {
    EEPROM.get(0, data);
}


void STMConfigAdaptor::sync(ConfigData &data) {
    EEPROM.put(0, data);
}
#endif
