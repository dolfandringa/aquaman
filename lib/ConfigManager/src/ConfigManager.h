#pragma once
#include <Arduino.h>
#include "ConfigData.h"
#include "BaseConfigAdaptor.h"
#ifdef USE_EEPROM_CONFIGADAPTOR
#include "EEPROMConfigAdaptor.h"
#endif
#ifdef USE_STM_CONFIGADAPTOR
#include "STMConfigAdaptor.h"
#endif

class ConfigManager {
    public:
        ConfigManager();
        virtual uint8_t getOwnAddress();
        virtual void setOwnAddress(uint8_t);
        virtual int16_t getFirstFreeAddress();
        virtual void setAddressInUse(uint8_t);
        virtual void freeAddress(uint8_t);
    protected:
        ConfigData data;
        void initializeData();
};

extern ConfigManager config;
