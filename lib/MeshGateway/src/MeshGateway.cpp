#include "MeshGateway.h"

MeshGateway::MeshGateway(){
    _config.setOwnAddress(0);
    _driver = new RH_NRF24(D4, D8);
    _manager = new RHMesh(*_driver, _config.getOwnAddress());
}

bool MeshGateway::begin() {
    return _manager->init();
}

bool MeshGateway::loop() {
    uint8_t len = sizeof(_buf);
    uint8_t from;
    if(_manager->recvfromAck(_buf, &len, &from)) {
        Serial.print("Got request from: 0x");
        Serial.print(from, HEX);
        Serial.println((char*)_buf);
    }
    return true;
}

MeshGateway::~MeshGateway() {
    delete _manager;
    delete _driver;
}
