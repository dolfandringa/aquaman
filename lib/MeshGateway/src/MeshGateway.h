#pragma once

#include <Arduino.h>
#include <RH_NRF24.h>
#include <RHMesh.h>
#include <ConfigManager.h>


class MeshGateway {
    public:
        MeshGateway();
        virtual bool begin();
        virtual bool loop();
        virtual ~MeshGateway();
    private:
        ConfigManager _config;
        RH_NRF24 *_driver;
        RHMesh *_manager;
        uint8_t _buf[RH_MESH_MAX_MESSAGE_LEN];
};
