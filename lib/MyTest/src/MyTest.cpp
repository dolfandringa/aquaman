#include "MyTest.h"

MyTest::MyTest(){ 
    _driver = new RH_NRF24(1, 1);
    _manager = new RHMesh(*_driver, 0);
};

void MyTest::begin(uint8_t a, uint8_t b) {
    _manager->init();
};
MyTest::~MyTest() {
    delete _manager;
    delete _driver;
};
