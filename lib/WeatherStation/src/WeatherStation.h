#include <Arduino.h>
#include <Wire.h>
#include "Adafruit_HTU21DF.h"
#include <Adafruit_BMP085.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

void setup_wifi(void);
void send_values(void);
void reconnect(void);
void setup(void);
void loop(void);
