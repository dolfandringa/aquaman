#include "WeatherStation.h"

#define INTERVAL 15*60*1000 // in milliseconds

Adafruit_HTU21DF htu = Adafruit_HTU21DF();
Adafruit_BMP085 bmp;
const char* ssid = "dolf";
const char* password = "b0OhBgP6PCi40XiEObAG";
const char* mqtt_server = "mqtt.dolf.lan";

WiFiClient espClient;
PubSubClient client(espClient);

uint64_t lastMsg = 0;

void setup_wifi() {
  delay(10);
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  WiFi.mode(WIFI_STA);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void send_values() {
  lastMsg = millis();
  digitalWrite(LED_BUILTIN, LOW);
  float temp = htu.readTemperature();
  float rel_hum = htu.readHumidity();
  float pres = bmp.readPressure();
  char msg[10];
  
  
  Serial.print("Pressure: "); Serial.print(pres); Serial.println(" Pa");
  Serial.print("Temp: "); Serial.print(temp); Serial.println(" C");
  Serial.print("Humidity: "); Serial.print(rel_hum); Serial.println(" \%");
  
  sprintf(msg, "%f.2", temp);
  client.publish("/sensors/temperature", msg);
  sprintf(msg, "%f.2", rel_hum);
  client.publish("/sensors/humidity", msg);
  sprintf(msg, "%f.2", pres);
  client.publish("/sensors/baropressure", msg);
  digitalWrite(LED_BUILTIN, HIGH);
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.begin(115200);
  
  setup_wifi();
  client.setServer(mqtt_server, 1883);

  if (!htu.begin()) {
    Serial.println("Couldn't find HTU sensor, check wiring!");
    while (1);
  }
  if (!bmp.begin()) {
    Serial.println("Could not find a valid BMP180 sensor, check wiring!");
    while (1);
  }
  
  
}

void loop() {
  if (!client.connected()) {
    reconnect();
    send_values();
  }
  
  uint64_t now = millis();
  if (now - lastMsg > INTERVAL) {
    send_values();
  }
  
  client.loop();
}
