#pragma once

#include <Arduino.h>
#include <ConfigManager.h>
#include <RH_NRF24.h>
#include <RHMesh.h>

class MeshClient {
    public:
        bool begin();
        bool loop();
        bool sendSensorValue(char* value, char* type);
        bool send(char* message, uint8_t address);
        void setCallback(void (*ptr)(char * message));
    private:
        RH_NRF24 *_driver;
        RHMesh *_manager;
        char _buf[RH_MESH_MAX_MESSAGE_LEN];
        void (*_callback)(char * message);
};
