#include "MeshClient.h"

bool MeshClient::begin() {
    config.setOwnAddress(0);
    _driver = new RH_NRF24(D4, D8);
    _manager = new RHMesh(*_driver, config.getOwnAddress());
    return _manager->init();
}

void MeshClient::setCallback(void (*ptr)(char* message)) {
    _callback = ptr;
}

bool MeshClient::sendSensorValue(char* value, char* type) {
    sprintf(_buf, "sensor;%s;%s", type, value);
    return MeshClient::send(_buf, 0);
}

bool MeshClient::send(char* message, uint8_t address) {
    return _manager->sendtoWait((uint8_t*)message, sizeof(message), address) == RH_ROUTER_ERROR_NONE;
}

bool MeshClient::loop() {
    uint8_t len = sizeof(_buf);
    uint8_t from;
    if (_manager->RHRouter::recvfromAckTimeout((uint8_t*) _buf, &len, 3000, &from)) {
        _callback(_buf);
    }
    return false;
}
