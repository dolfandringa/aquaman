#include "WaterLevelSensor.h"
#include <Arduino.h>

MeshGateway gateway;

void setup() {
    Serial.begin(9600);
    if(!gateway.begin()) {
        Serial.println("Failed to initialize radio");
        ESP.deepSleep(60e6);
    }
}

void loop() {
    if(gateway.loop()) {
        Serial.println("We received a message");
    }
}
